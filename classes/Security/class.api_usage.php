<?php 
	class api_usage {
		var $db = null;
		
		function api_usage($db) {
			$this->db = $db;
		}
		
		/**
		 * Obtiene la autorización para el uso de la api en funcion de la api key
		 * @param array $filters se recibe un array con 2 posiciones
		 * en la primera posición tenemos las keys generadas con anade_filtrado
		 * en la segunda posicion tenemos los values en un array 
		 * del tipo [':campo'] => 'valor'
		 * @param string $order
		 */
		function authorize($filters) {
			$ro = new Response();
			$ro->resultado = true;
			
			//preparo la query
			$filtros = prepare_filters($filters['keys']);
			$this->db->query("SELECT * FROM api_user ".$filtros." ;");
			$this->db->prebind($filters['values']);
			
			//la ejecuto
			$rows = $this->db->resultset();
			
			//proceso los datos
			if(!is_array($rows) || sizeof($rows) == 0) {
				$ro->resultado = false;
			}
			
			return $ro;
		}

        /**
         * Método que se encarga de validar si la API puede ser usada o no
         * @param $app: variable de la aplicación, donde se guardan todos los métodos rest
         * @param $db: variable de base de datos, para poder comprobar la autorización
         * @return bool
         */
        function is_authorized($app){
            $app->hook('slim.before.dispatch', function () use ($app){
                $db = new db();
                //cargo la variable desarrollo
                $development        = unserialize(DEVELOPMENT);
                $forDocumentation   = false;

                $router   = $app->router();

                if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] === ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')?"https":"http")."://".$_SERVER['SERVER_NAME']."/doc/"){
                    $forDocumentation = true;
                }else if(isset($_SERVER['HTTP_REFERER']) && explode("?",$_SERVER['HTTP_REFERER'])[0] === ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')?"https":"http")."://".$_SERVER['SERVER_NAME']."/dist/thumbnail.php"){
                    $forDocumentation = true;
                }

                //si no estamos en desarrollo, y no nos están pidiendo que mostremos la documentación
                if(!$development['enabled'] && !in_array($router->getCurrentRoute()->getPattern(), array("/","/doc/")) && !$forDocumentation) {
                    //obtengo el parámetro de la key que me tiene que venir como parámetro en el header
                    $headers    = apache_request_headers();

                    if(isset($headers['Authenticate']) and !empty($headers['Authenticate'])) {
                        $keyToCheck = $headers['Authenticate'];

                        //añado los filtros
                        $api_filter = array();
                        add_filter($api_filter, "apikey", $keyToCheck);
                        add_filter($api_filter, "enabled", 1);

                        $authorized = $this->authorize($api_filter);
                    }else{
                        $authorized = new Response();
                        $authorized->resultado = false;
                    }

                    if (!$authorized->resultado) //Si no tengo acceso peto avisando
                        $app->halt('403', get_error(0));
                }
            });

            return true;
        }

        /**
         * Método que se encarga de comprobar si todos los parámetros requeridos para el uso de un método han sido recibidos
         * @param $app: variable de la aplicación, donde se guardan todos los métodos rest
         * @return bool
         */
        function required_params_received($app){
            $app->hook('slim.before.dispatch', function () use ($app) {
                //necesito comprobar que me envían todas las variables necesarias para los métodos post
                $router = $app->router();
                $routes = $router->getAllRoutes();

                foreach ($routes as $route) {
                    //obtengo la información del método de la clase que se encarga de la función de la API
                    $method = new ReflectionMethod($route['class'], $route['method']);

                    //si la url coincide con la que nos han solicitado
                    if ($route['url'] === $router->getCurrentRoute()->getPattern()) {
                        //relleno la información que me da el PHPdoc
                        $api_method = array(
                            "title"    => getDocComment($method->getDocComment(), "@title")
                            ,"method"   => $route['http_method']
                            ,"params"   => getDocComment($method->getDocComment(), "@param")

                        );

                        //limpio los parámetros para comprobar después los requeridos (si el método tiene parámetros)
                        if (!empty($api_method['params'])) {
                            $params               = (is_array($api_method['params']))?$api_method['params']:array($api_method['params']);
                            $api_method['params'] = array();
                            foreach ($params as $key => $value) {
                                $param = explode("|", utf8_encode(str_replace("'", '', $value)));
                                $api_method['params'][trim($param[0])] = (trim($param[2]) === 'required')?true:false;
                            }
                        }else{
                            return true;
                        }

                        break;
                    }
                }

                //obtengo los parámetros que me envían
                $received_params = (strtoupper($api_method['method']) === 'GET')?$app->request->get():$app->request->post();

                $message = "";
                //recorro todos los parámetros para ver si me envían los requeridos
                foreach($api_method['params'] as $parameter=>$required){
                    if($required && !array_key_exists($parameter, $received_params)){
                        $message .= $parameter.",";
                    }
                }

                //si algun parámetro requerido no ha sido recibido, no se puede usar la API
                if(!empty($message)){
                    $app->halt('412', str_replace("[PARAMS]", trim($message,","), get_error(1)));
                }
            });

            return true;
        }
		
	} // class
		
?>