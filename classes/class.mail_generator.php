<?php
class Mail_generator{
	
	/**
	 * Une el texto creado al esqueleto
	 * @param string $cuerpo: cuerpo del mail
	 */
	function ensamblar($cuerpo){
		//obtengo el esqueleto
		$esqueleto = file_get_contents($_SERVER['DOCUMENT_ROOT']."/mailing/esqueleto.html");
		
		//preparo los campos a rellenar
		$search  = array("[TEXTO]");
		$replace = array($cuerpo);
		
		//a�ado el cuerpo del texto y los colores de las dem�s cosas
		$mail    = str_replace($search, $replace, $esqueleto);
		
		return $mail;
	}
}