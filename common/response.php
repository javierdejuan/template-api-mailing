<?php
	function get_error($code){
		$errors = array(
			 "0" => array(
						"status_code" 	=> 403
						,"message" 		=> "You don't have permissions to use our Api."
						,"error_code"	=> 1991
					),
             "1" => array(
                         "status_code" 	=> 412
                        ,"error" 		=> true
                        ,"message" 		=> "Next parameters are required and were not received: [PARAMS]."
                        ,"error_code"	=> 1991
             ),
			 "2" => array(
						"status_code" 	=> 200
						,"message" 		=> "Error enviando el email de backup."
						,"error_code"	=> 1992
					)
		);
		return json_encode($errors[$code]);
	}
	
	function get_ok($code){
		$errors = array(
			 "1" => array(
						"status_code" 	=> 200
						,"message" 		=> "Message OK"
					)
		);
		return json_encode($errors[$code]);
	}

    function print_mail($html){
        header('Content-Type: text/html; charset=ISO-8859-1');
        ob_clean();
        exit($html);
    }