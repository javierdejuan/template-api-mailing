<?php
    $hostname 	= strtolower(gethostname());
    $lander 	= "/lander/i";
    $frontpre 	= "/frontpre/i";

	//codificaci�n del site (iso -> mssql / utf8 -> eoc)
	define("CODIFICACION", "UTF-8");

    //DB CONSTANTS
    define("DB_TYPE", "mysql"); //mysql/sqlsvr

    if (preg_match($lander, $hostname)){
        //echo "lander";
        define("DB_HOST", "");
        define("DB_USER", "");
        define("DB_PASS", "");
        define("DB_NAME", "");

        //develpment potencia el debugeo de la aplicaci�n y configura cosas como el env�o de emails
        define("DEVELOPMENT", serialize(
            array(
                 'enabled'			=> true
                ,'development_mail'	=> ""
                ,'querys'			=> true
                ,'query_params'		=> true
                ,'query_result'		=> false
            ))
        );

        //host para enviar mails
        define("MAILER_HOST", "52.50.119.35");
    }else if (preg_match($frontpre, $hostname)){
        //echo "FRONTPRE";
        define("DB_HOST", "");
        define("DB_USER", "");
        define("DB_PASS", "");
        define("DB_NAME", "");

        //develpment potencia el debugeo de la aplicaci�n y configura cosas como el env�o de emails
        define("DEVELOPMENT", serialize(
            array(
                 'enabled'			=> true
                ,'development_mail'	=> ""
                ,'querys'			=> false
                ,'query_params'		=> false
                ,'query_result'		=> false
            ))
        );

        //host para enviar mails
        define("MAILER_HOST", "10.159.8.205");
    }else{
        //echo "OTRO";
        define("DB_HOST", "");
        define("DB_USER", "");
        define("DB_PASS", "");
        define("DB_NAME", "");

        //develpment potencia el debugeo de la aplicaci�n y configura cosas como el env�o de emails
        define("DEVELOPMENT", serialize(
            array(
                 'enabled'			=> false
                ,'development_mail'	=> ""
                ,'querys'			=> false
                ,'query_params'		=> false
                ,'query_result'		=> false
            ))
        );

        //host para enviar mails
        define("MAILER_HOST", "10.159.8.205");
    }

	//hash de encriptaci�n
	define("ENCRYPT_HASH", "el despotricador cinefilo rules!");
	
	//gesti�n de todo el m�dulo de mantenimiento
	define("MAINTENANCE",	serialize(
		 array(
			 'enabled' 			=> false
			,'allowed_ips' 		=> array()
			,'message'			=> "La web se encuentra en mantenimiento, sentimos<br>las molestias."
			,'company'			=> 'Innova D.S.'
			//,'twitter'		=> '@company' a configurar en /mantenimiento/php/get-tweets.php 
			,'year'				=> "2015"
			,'month'			=> "06"
			,'day'				=> "09"
			,'hour'				=> "0"
			,'minute'			=> "0"
			,'second'			=> "02"
			,'about_header'		=> "Marcamos la diferencia"
			,'about_text'		=> "Lorem fistrum incididunt esse cillum reprehenderit magna a peich pecador exercitation commodo. Pecador duis aliqua cillum no puedor amatomaa llevame al sircoo. Incididunt apetecan laboris papaar papaar magna. Amatomaa reprehenderit amatomaa magna sit amet sexuarl. Adipisicing pecador a peich no te digo trigo por no llamarte Rodrigor a wan mamaar ahorarr se calle ust�e hasta luego Lucas magna."
			,'address'			=> "C/ Arturo Soria 262 Bajo Izq &#183; Madrid"
			,'telephone'		=> "914126884"
			,'email'			=> "soporte@innovadsl.es"//este email es el que sacamos en la info
			,'maintenance_email'=> "javier.djt@innovadsl.es"//este mail recibe los correos del formulario de contacto
			,'facebook_url'		=> "https://facebook.com/innovadsl"
			,'twitter_url'		=> "https://twitter.com/innovadsl"
			,'gplus_url'		=> "https://plus.google.com/innovadsl"
			,'pinterest_url'	=> "https://pinterest.com/innovadsl"
			,'youtube_url'		=> "https://youtube.com/innovadsl"
			,'linkedin_urk'		=> "https://linkedin.com/innovadsl"
		))
	);
	
	//carpeta temporal para hacer cosas
	define("TMP_DIR", "tmp");
	
	//carpeta de archivos subidos
	define("UPLOAD_DIR", "uploads");
	
	//tama�o de los thumbnails
	define("THUMBNAIL_WIDTH", "236");
	define("THUMBNAIL_HEIGHT", "236");