<?php
    header('Content-Type: text/html; charset=UTF-8');
?>
<html lang="e">
  <head>
    <meta charset="<?=CODIFICACION?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Mail-Api ICEMD</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="http://www.icemd.com/app/themes/icemd_2014/favicon.ico">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.red-indigo.min.css" />
    <link rel="stylesheet" href="/dist/css/styles.css">
      <link rel="stylesheet" href="/dist/css/dialog.css">
  </head>
  <body class="mdl-demo mdl-color--grey-100 mdl-color-text--grey-700 mdl-base">
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
      <header class="mdl-layout__header mdl-layout__header--scroll mdl-color--primary">
        <div class="mdl-layout--large-screen-only mdl-layout__header-row">
        </div>
        <div class="mdl-layout--large-screen-only mdl-layout__header-row">
          <h3>Mailing ICEMD</h3>
        </div>
        <div class="mdl-layout--large-screen-only mdl-layout__header-row">
        </div>
        <div class="mdl-layout__tab-bar mdl-js-ripple-effect mdl-color--primary-dark">
          <a href="#inicio" id="old-home" class="mdl-layout__tab is-active">Inicio</a>
          <?php foreach($documentation as $key=>$doc){?>
              <a href="#<?=strtolower($key)?>" id="link_<?=strtolower($key)?>" class="mdl-layout__tab"><?=str_replace("_"," ",$key)?></a>
          <?php }?>
          <div class="mdl-layout-spacer"></div>
          <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable" style="position:absolute;right:20px;">
            <label class="mdl-button mdl-js-button mdl-button--icon" for="search">
                <i class="material-icons">search</i>
            </label>
            <div class="mdl-textfield__expandable-holder">
                <input class="mdl-textfield__input" type="text" id="search" placeholder="Buscar">
                <label class="mdl-textfield__label" for="search">Enter your query...</label>
            </div>
            </div>
        </div>
      </header>
      <main class="mdl-layout__content">
        <div class="mdl-layout__tab-panel is-active" id="inicio">
            <section id="help" class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
                <div class="mdl-card mdl-cell mdl-cell--12-col">
                    <div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">
                        <h4 class="mdl-cell mdl-cell--12-col">Detalles de uso Api-Mailing de <img width="80px" src="/dist/images/icemd-logo.png" style="margin-top: 5px;"></h4>
                        <div class="section__circle-container mdl-cell mdl-cell--2-col mdl-cell--1-col-phone">
                            <div class="section__circle-container__circle">
                                <button data-upgraded=",MaterialButton,MaterialRipple" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-shadow--4dp mdl-color-red" id="setting">
                                    <i class="material-icons" role="presentation">lock</i>
                                    <span class="visuallyhidden">lock</span>
                                    <span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span>
                                </button>
                            </div>
                        </div>
                        <div class="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
                            <h5>Uso permitido de la API</h5>
                            Toda petición de plantilla de email debe estar firmada con la <b>apiKey</b> suministrada por ICEMD y ser enviada mediante una variable de cabecera con la key <b>Authenticate</b>.
                        </div>
                        <div class="section__circle-container mdl-cell mdl-cell--2-col mdl-cell--1-col-phone">
                            <div class="section__circle-container__circle">
                                <button data-upgraded=",MaterialButton,MaterialRipple" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-shadow--4dp mdl-color-red" id="setting">
                                    <i class="material-icons" role="presentation">settings</i>
                                    <span class="visuallyhidden">Settings</span>
                                    <span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span>
                                </button>
                            </div>
                        </div>
                        <div class="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
                            <h5>Algunos Mails necesitan variables de configuración</h5>
                            Algunos Mails necesitan variables para ser más completos y generar el mail deseado, revisar bien la documentación para comprender los parámetros necesarios.
                        </div>
                        <div class="section__circle-container mdl-cell mdl-cell--2-col mdl-cell--1-col-phone">
                            <div class="section__circle-container__circle">
                                <button data-upgraded=",MaterialButton,MaterialRipple" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-shadow--4dp mdl-color-red" id="setting">
                                    <i class="material-icons" role="presentation">line_style</i>
                                    <span class="visuallyhidden">line style</span>
                                    <span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span>
                                </button>
                            </div>
                        </div>
                        <div class="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
                            <h5>Completa tu plantilla reemplazando las Keywords</h5>
                            Una vez obtengas tu plantilla, deberás cambiar las keywords por los datos reales antes de enviar el email al destinatario.
                        </div>
                    </div>
                </div>
                <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" id="btn-help">
                    <i class="material-icons">clear</i>
                </button>
            </section>
            <br><br>
        </div>
          <?php $cont = 0; foreach($documentation as $key2=>$seccion){?>
              <div class="hidden mdl-layout__tab-panel" id="<?=strtolower($key2)?>">
                  <?php foreach($seccion as $key=>$doc){?>
                      <section data-title="<?=$doc['title']?>" data-parent="<?=strtolower($key2)?>" class="method section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
                          <header style="border-right:1px solid #d1d1d1;" class="section__play-btn mdl-cell mdl-cell--2-col-desktop mdl-cell--2-col-tablet mdl-cell--4-col-phone mdl-color--teal-100 mdl-color-text--white">
                              <iframe class="iframe" scrolling="no" data-src="/dist/thumbnail.php?url=<?=urlencode("http://".$_SERVER['SERVER_NAME'].$doc['url'])?>" src="" frameborder="0" height="100%" style="overflow:hidden;width:143px;display:none;"></iframe>
                          </header>
                          <div class="mdl-card mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--4-col-phone">
                              <div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">
                                  <h4 class="mdl-cell mdl-cell--12-col"><?=$doc['title']?></h4>
                                  <div class="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
                                      <h5>Descripción</h5>
                                      <?=$doc['desc']?>
                                  </div>
                                  <div class="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
                                      <h5>Petición</h5>
                                      <?="http://".$_SERVER['SERVER_NAME'].$doc['url']?>
                                  </div>
                                  <div class="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
                                      <h5>Método</h5>
                                      <?=$doc['method']?>
                                  </div>
                                  <?php if((is_array($doc['params']) && sizeof($doc['params'])>0) || (is_string($doc['params']) && $doc['params']!='')){?>
                                      <div class="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
                                          <h5>Parámetros</h5>
                                          <ul class="demo-list-icon mdl-list" style="margin-left: 20px;">
                                              <?php
                                              if(is_array($doc['params'])){
                                                  foreach($doc['params'] as $param){?>
                                                      <li class="mdl-list__item parameter param_<?=$cont?> <?=(trim($param[2]) === 'required')?"required":""?>">
                                                              <span class="mdl-list__item-primary-content title" >
                                                                <i class="material-icons mdl-list__item-icon">expand_more</i>
                                                                <i style="display: none" class="material-icons mdl-list__item-icon">expand_less</i>
                                                                  <b><?=ucfirst(strtolower(trim($param[0])))?></b> <?=(trim($param[2]) === 'required')?"<sup>*</sup>":""?>
                                                              </span>
                                                          <div>
                                                              <div class="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
                                                                  <h5>Parámetro</h5>
                                                                  <?=strtolower(trim($param[0]))?>
                                                              </div>
                                                              <div class="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
                                                                  <h5>Descripción</h5>
                                                                  <?=ucfirst(utf8_decode($param[4]))?>
                                                              </div>
                                                              <div class="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
                                                                  <h5>Tipo</h5>
                                                                  <?=ucfirst(trim($param[1]))?>
                                                              </div>
                                                              <div class="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
                                                                  <h5>Requerido</h5>
                                                                  <?=(trim($param[2]) === 'required')?"Sí":"No"?>
                                                              </div>
                                                              <div class="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
                                                                  <h5>Ejemplo</h5>
                                                                  <?=trim($param[3])?>
                                                              </div>
                                                          </div>
                                                      </li>
                                                  <?php }
                                              }else{?>
                                                  <?=$doc['params']?><br/>
                                              <?php }?>
                                          </ul>
                                      </div>
                                  <?php }?>
                              </div>
                              <div class="mdl-card__actions">
                                  <a data-url="<?=(isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] !== 'off')?"https":"http")."://".$_SERVER['SERVER_NAME'].$doc['url']?>" class="template-url mdl-button">Visualizar plantilla</a>
                              </div>
                          </div>
                          <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" id="btn<?=$cont?>">
                              <i class="material-icons">more_vert</i>
                          </button>
                          <ul class="mdl-menu mdl-js-menu mdl-menu--bottom-right" for="btn<?=$cont?>">
                              <li class="mdl-menu__item param_filter param_filter_<?=$cont?>" data-key="<?=$cont?>" style="min-width: 220px;"><label>Mostrar solo parámetros requeridos</label><br><label style="display:none;">Mostrar todos los parámetros</label></li>
                          </ul>
                      </section>
                  <?php }?>

                  <section class="section--footer mdl-color--white mdl-grid">
                      <div class="section__circle-container mdl-cell--1-col-desktop mdl-cell--1-col-phone">
                          <div class="section__circle-container__circle mdl-color--primary-dark section__circle--big"></div>
                      </div>
                      <div class="section__text mdl-cell mdl-cell--4-col-desktop mdl-cell--7-col-tablet mdl-cell--3-col-phone">
                          <h5>Llamada desde <b>Php</b></h5>
                          La manera más óptima de hacer uso de esta api con el lenguaje Php es mediante una petición Curl, aquí tienes un ejemplo:
                          <pre><code>
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, '<?="http://".$_SERVER['SERVER_NAME']."/doc/"?>'');
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authenticate: M05J12<3SST!'));
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$data = curl_exec($ch);
curl_close($ch);
                          </code></pre>

                      </div>
                      <div class="section__circle-container mdl-cell--1-col-desktop mdl-cell--1-col-phone">
                          <div class="section__circle-container__circle mdl-color--primary-dark section__circle--big"></div>
                      </div>
                      <div class="section__text mdl-cell mdl-cell--4-col-desktop mdl-cell--7-col-tablet mdl-cell--3-col-phone">
                          <h5>Llamada desde <b>ColdFusion</b></h5>
                          La manera más óptima de hacer uso de esta api con el lenguaje ColdFusion es mediante una petición CfHttp, aquí tienes un ejemplo:
                          <pre><code>
&lt;cfhttp method="post" url="<?="http://".$_SERVER['SERVER_NAME']."/doc/"?>" result="templateMail"&gt;
    &lt;cfhttpparam type="header" name="Authenticate" value="M05J12<3SST!"/&gt;
&lt;/cfhttp&gt;
&lt;cfoutput>#templateMail.FileContent#&lt;/cfoutput&gt;
                           </code></pre>
                      </div>
                  </section>
              </div>
          <?php }?>

      </main>
        <footer class="mdl-mini-footer">
            <div class="lander">
                <ul class="mdl-mini-footer__link-list">
                    <li>
                        <a href="http://www.lander.es">
                            <img src="/dist/images/lander-logo.jpg" style="height: 20px;margin-left:5px;">
                        </a>
                    </li>
                </ul>
            </div>
        </footer>
    </div>
    <script src="//code.getmdl.io/1.1.3/material.min.js"></script>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="/dist/js/dialog.js"></script>
    <script src="/dist/js/doc.js"></script>
  </body>
</html>
