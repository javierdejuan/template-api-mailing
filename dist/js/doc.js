$(function(){
    $(".mdl-layout__tab").on("click", function(){
        var tab = $(this).attr("id").replace("link_","");
        $("#"+tab+" .iframe").each(function(){
            if($(this).attr("src")==='') {
                $(this).attr("src", $(this).data("src"));
            }

            $(this).load(function() {
                $(this).show();
            });
        });
    });

    $("#btn-help").click(function(){
        $("#old-home").fadeOut("slow");
        $("#link_autoresponder").addClass("is-active");
        $("#autoresponder").addClass("is-active");
        $("#inicio").removeClass("is-active");
    });

    //evento del buscador para ayudar a la b�squeda de documentaci�n
    $("#search").on("change keyup blur",function(){
        var cadena = $(this).val();
        if($(this).val()!='') {
            $(".method").each(function(){
                if($(this).data("title").toLowerCase().indexOf(cadena) != -1){
                    $(this).fadeIn("slow");
                    $("#link_"+$(this).data("parent")).css("background","rgba(255,255,255,0.5)");
                }else{
                    $(this).fadeOut("slow");
                    var $this = $(this);
                    var cont  = 0;
                    $("#"+$(this).data("parent")+" .method").each(function(){
                        if($(this).css('display') !== 'none'){
                            cont++;
                        }
                    })
                    if(cont === 0){
                        $("#link_"+$(this).data("parent")).css("background","");
                    }
                }
            })
        }else{
            $(".method").fadeIn("slow");
            $(".mdl-layout__tab").css("background","");
        }
    });

    $(".parameter .title").on("click", function(){
        $(this).siblings().slideToggle("slow");
        $(this).find(".material-icons").toggle();
    });

    $(".param_filter").on("click", function(){
        $(" .param_filter_"+$(this).data("key")+" label, .param_filter_"+$(this).data("key")+" br").fadeToggle("fast");

        if($(".param_"+$(this).data("key")).is(":hidden")){
            $(".param_"+$(this).data("key")).show();
        }else{
            $(".param_"+$(this).data("key")).hide();
            $(".param_"+$(this).data("key")+".required").show();
        }
    });

    $(".template-url").click(function(){
        showDialog({
            text: '<iframe height="100%" frameborder="0" scrolling="yes" src="'+$(this).data('url')+'" onload="this.style.opacity = 1" style="background:rgba(255, 255, 255, 1) url(\'/dist/images/loading.gif\') no-repeat scroll 50% 50% / 50% 30%;overflow: hidden; opacity: 1; width: 100%; min-height: 100%;"></iframe>',
            positive: {
                title: 'Cerrar',
            },
            contentStyle: {
                background: "rgba(255, 255, 255, 1) url('/dist/images/loading.gif') no-repeat scroll 50% 50% / 50% 30%"
            }
        });

    });
});

$(window).load(function(){
    $(".hidden").removeClass("hidden");
});