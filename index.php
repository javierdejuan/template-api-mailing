<?php
	require_once 'config/config.php'; //cerebro del sistema
	require_once 'lib/Slim/Slim.php'; // framework api rest
	require_once 'common/response.php'; // api errors
    require_once 'classes/class.mail_generator.php'; //template mail generator
	
	//iniciamos la api
	\Slim\Slim::registerAutoloader();
	$app 	 = new \Slim\Slim();

    //cargamos los archivos con los m�todos de los subsistemas
    require_once __DIR__ . '/api_content/class.documentation.php';          //subsistema de documentacion
    require_once __DIR__ . '/api_content/class.autoresponder.php';          //subsistema de emails de autorespuesta
    require_once __DIR__ . '/api_content/class.vigilantes.php';             //subsistema de emails para vigilantes
    require_once __DIR__ . '/api_content/class.inscripciones.php';          //subsistema de emails de inscripci�n
    require_once __DIR__ . '/api_content/class.icemdylatam.php';          //subsistema de emails de las webs corportativas de ICEMD

    //M�todos declarados de la api
    include_once __DIR__."/api_content/features.php";

    //si hay acceso, arrancamos la api
    if($api_usage->is_authorized($app, $db) && $api_usage->required_params_received($app))
        $app->run();
